{-#LANGUAGE GADTs, TypeOperators, FlexibleContexts, DataKinds, RankNTypes #-}
module Control.Effects.Logging where

import Control.Monad.Freer
-- todo use Text
data Logging a where
    Log :: Int -> String -> Logging ()

logMessage :: Member Logging effs => Int -> String -> Eff effs ()
logMessage loglevel msg = send (Log loglevel msg)

runLoggingStdOut :: Member IO r => Int -> Eff (Logging ': r) ~> Eff r
runLoggingStdOut maxLevel = interpret act where
    act :: (Member IO r) => Logging ~> Eff r
    act (Log lvl msg) = do
        if lvl <= maxLevel then send (putStrLn msg) else return ()

logError :: Member Logging effs => String -> Eff effs ()
logError = logMessage 0