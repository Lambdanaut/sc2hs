{-#LANGUAGE NoMonadFailDesugaring #-}
{-#LANGUAGE OverloadedLabels, TypeOperators, DataKinds, MonoLocalBinds #-}
{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleContexts #-}
module Main where

import Network.SC2.LowLevel hiding (getStatus, Step)
import qualified Proto.S2clientprotocol.Sc2api as A
import qualified Proto.S2clientprotocol.Common as A
import qualified Proto.S2clientprotocol.Sc2api_Fields as A
import qualified Proto.S2clientprotocol.Raw as A
import qualified Proto.S2clientprotocol.Raw_Fields as A

import Lens.Labels.Unwrapped ()
import Data.ProtoLens (defMessage)
import Control.Lens
import Control.Effects.Logging
import Network.SC2.Agent 

main :: IO ()
main = withMain mempty (runLocal  (runLoggingStdOut 999 . runSC2 bot) ) where   
  

bot :: SC2LowLevel '[Logging] ()
bot = do
  Right info <- syncRequest Ping
  logMessage 1 ("info: " ++ show info)

  Right maps@(map : _) <- syncRequest AvailableMaps

  Right () <- syncRequest $ CreateGameFull (maps !! 8) [Participant Protoss, Computer (Random ()) Medium] Fog RandomSeed Realtime
  logMessage 1 "create game success"

  Right pid <- syncRequest $ JoinGame Protoss [Raw]
  logMessage 1 ("join game: " ++ show pid)

  Right info <- syncRequest $ GameInfo
  runSC2Agent (initialiseWithGameInfo info) $ testAgent  
  

-- Orders a worker rush on the first frame.
testAgent :: Member Logging r =>   Eff (Agent ':r ) ()
testAgent = do
  updateObservations
  units <- workers
  locs <- enemyStartLocations
  attack units (head locs)
  logMessage 1 "Worker rush ^_^"
  loop
  
loop :: Member Logging r =>  Eff (Agent ': r) ()
loop = do
  step
  status <- getStatus
  case status of
    Ended -> logMessage 1 "Ending game"
    _     -> loop
