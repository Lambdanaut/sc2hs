{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE RankNTypes #-}
{-#LANGUAGE StandaloneDeriving #-}
{-#LANGUAGE FlexibleInstances#-}
module Network.SC2.Constants.Effects where 
    
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import GHC.TypeLits

import Network.SC2.Internal.ConstantGenerator

$(scEffectsDeclarations)