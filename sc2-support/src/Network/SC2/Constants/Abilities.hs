{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE RankNTypes #-}
{-#LANGUAGE StandaloneDeriving #-}
{-#LANGUAGE FlexibleInstances#-}
module Network.SC2.Constants.Abilities where 
    
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import GHC.TypeLits

import Network.SC2.Internal.ConstantGenerator

$(scAbilitiesDeclarations)